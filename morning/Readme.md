# Exercises:

## Commands: touch, mkdir, echo, cd,  

### Create a directory, then a file using touch within in: 

```
mkdir exercise
cd exercise
touch  exc
```
## Commands: touch, ls 
### Create a hidden directory, and a hidden file within it, list the contents with ls -a

```
mkdir .secret
cd .secret
touch .topsecret
ls -a
```


